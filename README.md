# CV-bVd
[![Netlify Status](https://api.netlify.com/api/v1/badges/c7e2a018-3720-4296-a4ea-f38464a511eb/deploy-status)](https://app.netlify.com/sites/glistening-custard-9451ab/deploys)

This project is made to display my cv in the browser

## link

- [website](http://cv.bvd.s3-website.eu-west-3.amazonaws.com/)
- [storybook](http://storybook-cv-bvd.s3-website.eu-west-3.amazonaws.com)


## Setup Project

| note: with newer node version be sure to add the following env variable `export NODE_OPTIONS=--openssl-legacy-provider`

### Git workflow

We use [git-flow][1] which implements [Vincent Driessen][2] branching model.  
The development happend on the default branch `main`.  
The `production`branch contains the productions releases.  
Install [git-flow-avh][1] following the instruction or through the package manager fo your choice

We advice to use following git-flow config:
```
Branch name for production releases: production
Branch name for "next release" development: main
Feature branch prefix: feature/
Bugfix branch prefix: bugfix/
Release branch prefix: release/
Hotfix branch prefix: hotfix/
Support branch prefix: support/
Version tag prefix: v
```

You can also setup the following parameters

```bash
git config gitflow.branch.master production
git config gitflow.branch.develop main
git config gitflow.prefix.feature feature/
git config gitflow.prefix.bugfix bugfix/
git config gitflow.prefix.release release/
git config gitflow.prefix.hotfix hotfix/
git config gitflow.prefix.support support/
git config gitflow.prefix.versiontag v
```

[1]: https://github.com/petervanderdoes/gitflow-avh
[2]: http://nvie.com/posts/a-successful-git-branching-model/

## Credits

This project uses several (open source)packages/technologies:

- Vue
- Vuetify
- AWS S3
- Docker
- Git/Gitlab
