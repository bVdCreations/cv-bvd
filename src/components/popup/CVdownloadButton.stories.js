import CVdownloadButton from "@/components/popup/CVdownloadButton"

export default {
  title: "CV/popup/CVdownloadButton",
  components: { CVdownloadButton }
}

const Template = (args, { argTypes }) => ({
  template: `
  <CVdownloadButton>
    <div id=dummy style="height: 200px; width: 100px">
    <p>container</p>
    </div>
  </CVdownloadButton>
  `,
  components: { CVdownloadButton },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
