import TitleCV from "@/components/CV/sub/titleCV"

export default {
  title: "CV/TitleCV",
  components: { TitleCV }
}

const Template = (args, { argTypes }) => ({
  template: `
  <titleCV
    :title="title"
    :subTitle="subTitle"
    :text="text"
  />
  `,
  components: { TitleCV },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
Main.args = {
  title: "Person Name",
  subTitle: "Curriculum Vitae",
  text:
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
}
