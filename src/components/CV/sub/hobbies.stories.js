import Hobbies from "@/components/CV/sub/hobbies"

export default {
  title: "CV/Hobbies",
  components: { Hobbies }
}

const Template = (args, { argTypes }) => ({
  template: `
  <hobbies
    :dataList="dataList"
    :title="title"
  />
  `,
  components: { Hobbies },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
Main.args = {
  title: "Hobbies",
  dataList: ["hobbie1", "hobbie2"]
}
