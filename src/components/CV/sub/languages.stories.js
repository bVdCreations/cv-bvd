import Languages from "@/components/CV/sub/languages"

export default {
  title: "CV/Languages",
  components: { Languages }
}

const Template = (args, { argTypes }) => ({
  template: `
  <languages
    :languages="languages"
    :title="title"
  />
  `,
  components: { Languages },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
Main.args = {
  title: "Languages",
  languages: {
    Dutch: {
      level: {
        Speaking: 1,
        Reading: 1,
        Writing: 1
      },
      text: "native"
    }
  }
}

const dataExample = {
  Dutch: {
    level: {
      Speaking: 1,
      Reading: 1,
      Writing: 1
    },
    text: "native"
  },
  English: {
    level: {
      Speaking: 0.9,
      Reading: 0.8,
      Writing: 0.75
    },
    text: "professional level"
  },
  French: {
    level: {
      Speaking: 0.5,
      Reading: 0.4,
      Writing: 0.3
    },
    text: "intermediate level"
  },
  Spanish: {
    level: {
      Speaking: 0.5,
      Reading: 0.5,
      Writing: 0.3
    },
    text: "beginner level"
  }
}

export const Full = Template.bind()
Full.args = {
  title: "Languages",
  languages: dataExample
}
