import QRcode from "@/components/atoms/QRcode"

export default {
  title: "CV/Atoms/QRcode",
  components: { QRcode }
}

export const Main = (args, { argTypes }) => ({
  template: "<QRcode/>",
  components: { QRcode },
  props: Object.keys(argTypes)
})
