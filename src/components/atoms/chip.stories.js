import Chip from "@/components/atoms/chip"

export default {
  title: "CV/Atoms/Chip",
  components: { Chip }
}

export const Template = (args, { argTypes }) => ({
  template: "<chip :text='text' />",
  components: { Chip },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
Main.args = {
  text: "chip text"
}
