import Vue from "vue"
import Vuetify, {
  VApp,
  VBtn,
  VCard,
  VToolbar,
  VToolbarTitle,
  VContainer,
  VList,
  VListItem,
  VListItemContent,
  VListItemTitle,
  VListItemSubtitle,
  VListItemAction,
  VIcon,
  VSubheader,
  VChip,
  VDivider,
  VDialog
} from "vuetify/lib"
import "vuetify/dist/vuetify.min.css"

import colors from "vuetify/es5/util/colors"

Vue.use(Vuetify, {
  components: {
    VApp,
    VBtn,
    VCard,
    VToolbar,
    VToolbarTitle,
    VContainer,
    VList,
    VListItem,
    VListItemContent,
    VListItemTitle,
    VListItemSubtitle,
    VListItemAction,
    VIcon,
    VSubheader,
    VChip,
    VDivider,
    VDialog
  }
})

const opts = {
  iconfont: "md" || "mdi" || "fa",

  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        topBarColor: colors.lightBlue.accent4,
        bottomBarColor: colors.lightBlue.accent1,
        backgroundColor: colors.blueGrey.lighten4,
        toolbarColor: colors.lightBlue.darken3,
        chipColor: colors.lightBlue.base,
        iconColor: colors.lightBlue.base,
        primary: "#ee44aa",
        secondary: "#424242",
        accent: "#82B1FF",
        error: "#FF5252",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107"
      }
    }
  }
}

export default new Vuetify(opts)
